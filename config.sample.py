BOT_PREFIX = "!"
BOT_TOKEN = "tokentokentoken"

GUILD_ID = 1234
CTF_CATEGORY_ID = 2345
ARCHIVE_CATEGORY_ID = 3456
BOT_CHANNEL_ID = 5678
MALWARE_CHANNEL_ID = 6789

ROLES = {"admin": [1234], "verified": 2345, "ctf": 3456, "aws": 4567}
ALLOWED_ROLES = ["ctf"]

EMBED_DEFAULT = 0x5C6BC0  # blue
EMBED_SUCCESS = 0x66BB6A  # green
EMBED_WARNING = 0xFFCA28  # yellow
EMBED_ERROR = 0xEF5350  # red

GITLAB_TOKEN = "tokentokentoken"
GITLAB_GROUP_ID = "123456"

DAAS_URL = "http://urlwithnoslash.com"
DAAS_API_KEY = ""

EMAIL_SERVER = "something.com"
EMAIL_USERNAME = "something@example.com"
EMAIL_PASSWORD = "hunter2"
VALID_EMAIL_DOMAINS = ["example.com"]

CDC_VPN_PRIVATE_KEY_PATH = "/path/to/.pem"
CDC_VPN_IP = "0.0.0.0"
CDC_VPN_USERNAME = "notroot"
CDC_VPN_SERVER_PUBKEY = "put+pub+key+here="
