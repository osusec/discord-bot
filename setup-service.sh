#!/bin/bash

if [ "$UID" == 0 ]; then
  echo "dont run me as root, ill prompt for sudo"
  exit 1
fi

echo "adding service to system"
echo "  will run as $USER"
echo "  in dir $PWD"

cat osusec-bot.example.service |
  sed "s|WorkingDir=.+|WorkingDir=$PWD|" |
  sed "s|User=.+|User=$USER|" |
  sudo tee /etc/systemd/system/osusec-bot.service

echo "enabling and starting service"

sudo systemctl daemon-reload
sudo systemctl enable --now osusec-bot.service

echo "see logs:  journalctl -u osusec-bot (-e|-f)"
echo "restart:   systemctl restart osusec-bot"
